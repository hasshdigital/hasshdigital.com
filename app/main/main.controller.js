'use strict';

var hdcApp = angular.module('main.controller', []);

hdcApp
 .controller('MainCtrl', [
  '$scope',
  '$route',
  function ($scope, $route){
   $scope.title='HasshDigital';
  }
 ]);